# Simple D3 React widget

See instructions below:

## Install

```
yarn install
```

## Start

```
yarn start
```


(Виджет "конвертер валют"

Описание задания [Конвертер валют](https://docs.google.com/document/d/1KWuOkO2jl_K1Btm5iVaqiPt0YM2uUuXNcfYdTMI6ZG0/edit#heading=h.7wggp6wbzt2a).

В данном конвертере конвертация производится по усредненному безналичному курсу (средняя цена валюты между ценой покупки и ценой продажи). Аналогично тому как это делается в Google-конверторе встроенном в поисковик (отображается при поисковом запросе типа "гривна курс").

Заголовок с курсами валют объединен с виджетом конвертера и является заголовком виджета.)

## Установка

```
yarn install
```

## Запуск

```
yarn start
```

## Тесты

```
yarn test
```

Основной тест - проверка работоспособности открытого API Приватбанка, и дополнительно небольшой тест интерфейса.
