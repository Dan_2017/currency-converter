import { render } from '@testing-library/react'
import { Provider } from 'react-redux'
import App from './App'
import { store } from './app/store'

test('Renders widget footer with API info', () => {
  const { getByText } = render(
    <Provider store={store}>
      <App />
    </Provider>,
  )

  expect(
    getByText(
      'Конвертация производится по усредненному безналичному курсу АО КБ "Приватбанк" https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11',
    ),
  ).toBeInTheDocument()
})
