import * as d3 from 'd3'
import React, { useCallback, useEffect, useRef, useState } from 'react'
import styles from './Wordchart.module.css'

export function Wordchart(props) {
  const d3Element = useRef()
  const barChart = useRef()
  const pieChart = useRef()
  const [text, setText] = useState(undefined)
  const [dataset, setDataset] = useState([])
  const [pieLabels, setPieLabels] = useState([])

  useEffect(() => {
    if (props.text && text === undefined) updateChart(props.text)
  })

  useEffect(() => {
    let color = d3.scaleOrdinal([
      '#4daf4a',
      '#377eb8',
      '#ff7f00',
      '#984ea3',
      '#e41a1c',
      '#d9e536',
    ])

    // D3 Barchart

    const rect_width = 15

    d3.select(barChart.current)
      .selectAll('rect')
      .data(dataset)
      .enter()
      .append('rect')

    d3.select(barChart.current)
      .selectAll('rect')
      .attr('fill', function (d, i) {
        return color(i)
      })
      .attr('x', (d) => 30)
      .attr('y', (d, i) => 5 + i * (rect_width + 7))
      .attr('width', (d) => d[2] * 10)
      .attr('height', (d) => rect_width)

    d3.select(barChart.current)
      .selectAll('rect')
      .transition()
      .duration(200)
      .attrTween('width', (d) => d3.interpolateNumber(d[1] * 10, d[2] * 10))

    d3.select(barChart.current).selectAll('rect').data(dataset).exit().remove()

    barChart.current.style.top = ((20 - dataset.length) / 2) * 22 + 'px'

    // D3 Piechart

    const data = dataset.map((d) => d[2])
    const svg = d3.select(pieChart.current)
    const width = svg.attr('width')
    const height = svg.attr('height')
    const radius = Math.min(width, height) / 3
    const cf = 0.5 + 0.05 * dataset.length
    const outerRadius = radius * cf
    const innerRadius = outerRadius - 20
    const _svg = document.querySelector('#pieChart')
    const arc = d3.arc().innerRadius(innerRadius).outerRadius(outerRadius)
    const _labels = []

    while (_svg.lastChild) _svg.removeChild(_svg.lastChild)

    const label = d3
      .arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius + 55)

    const count = d3
      .arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius + 102)

    const arcs = svg.selectAll('arc').data(d3.pie()(data)).enter().append('g')

    arcs
      .append('path')
      .attr('fill', function (d, i) {
        return color(i)
      })
      .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')
      .attr('d', arc)

    arcs.append('text').attr('transform', function (d, i) {
      _labels.push([
        width / 2 + label.centroid(d)[0] - 5,
        height / 2 + label.centroid(d)[1] + 4,
        dataset[i][0],
        width / 2 + count.centroid(d)[0] - 5,
        height / 2 + count.centroid(d)[1] + 4,
        dataset[i][2],
      ])
    })

    setPieLabels(_labels)
  }, [dataset, setDataset, setPieLabels])

  const updateChart = useCallback(
    (textData) => {
      let prevs = {}
      dataset.map((d) => (prevs[d[0]] = d[2]))
      const chars = new Set(textData.split('').sort())
      const counts = textData
        .split('')
        .sort()
        .reduce((map, val) => {
          map[val] = (map[val] || 0) + 1
          return map
        }, {})
      const newDataset = Array.from(chars).map((d) => [
        d,
        prevs[d] || 0,
        counts[d],
      ])
      setDataset(newDataset)
      setText(textData)
    },
    [dataset],
  )

  const barChartLabels = dataset.map((d, i) => (
    <React.Fragment key={i}>
      <span className={styles['caption']}>{d[0]}</span>
      <span
        className={styles['count']}
        style={{
          left: d[2] * 10 + 35 + 'px',
          top: 3 + i * 22 + 'px',
        }}
      >
        {d[2]}
      </span>
    </React.Fragment>
  ))

  const pieChartLabels = pieLabels.map((d, i) => (
    <React.Fragment key={i}>
      <span
        className={styles['count']}
        style={{
          position: 'absolute',
          top: d[4] - 14 + 'px',
          left: d[3] - 6 + 'px',
          transition: 'none',
        }}
      >
        {d[5]}
      </span>
      <span
        className={styles['caption']}
        style={{
          position: 'absolute',
          top: d[1] - 16 + 'px',
          left: d[0] - 8 + 'px',
          borderRadius: '10px',
        }}
      >
        {d[2]}
      </span>
    </React.Fragment>
  ))

  return (
    <div className={styles['widget']} style={{ position: 'absolute' }}>
      <div className={styles['widget-header']}>Simple React D3 Example</div>
      <div className={styles['widget-content']}>
        <div
          className={styles['bar-chart-labels']}
          style={{
            top: ((20 - dataset.length) / 2) * 22 + 'px',
          }}
        >
          {barChartLabels}
        </div>
        <div
          className={styles['pie-chart-labels']}
          style={{
            top: 125 - 0.5 + 0.05 * dataset.length + 'px',
            left: 260 - (0.5 + 2.5 * dataset.length) + 'px',
          }}
        >
          {pieChartLabels}
        </div>
        <div ref={d3Element} className={styles['d3Element']}>
          <svg
            className={styles['barChart']}
            ref={barChart}
            width="250"
            height="450"
            style={{
              left: '30px',
              top: ((20 - dataset.length) / 2) * 22 - 5 + 'px',
            }}
          />
          <svg
            className={styles['pieChart']}
            ref={pieChart}
            id="pieChart"
            width="200"
            height="200"
            style={{
              left: 260 - (0.5 + 2.5 * dataset.length) + 'px',
              top: 125 - 0.5 + 0.05 * dataset.length + 'px',
            }}
          />
        </div>
      </div>
      <div className={styles['widget-footer']}>
        <input
          value={text}
          className={styles['widget-input']}
          placeholder="type any text here"
          maxLength="20"
          onChange={(e) => {
            updateChart(e.target.value)
          }}
        />
      </div>
    </div>
  )
}
