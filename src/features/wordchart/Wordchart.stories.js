import React from 'react'

import { Wordchart } from './Wordchart'

export default {
  title: 'Test/Wordchart',
  component: Wordchart,
  argTypes: {
    text: { control: 'text' },
  },
}

const Template = (args) => <Wordchart {...args} />

export const Primary = Template.bind({})
Primary.args = {
  text: '',
}

export const Secondary = Template.bind({})
Secondary.args = {
  text: 'React D3 example',
}
