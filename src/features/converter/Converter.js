import { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  fetchRatesAsync,
  selectError,
  selectRates,
  selectStatus,
  setStatus,
} from './converterSlice'

import './Converter.css'
import styles from './Converter.module.css'

import flags from './assets/flags.png'
import ratesSourceLogo from './assets/rates-source-logo.svg'

export function Converter() {
  const baseCurrency = useRef({
    ccy: 'UAH',
    base_ccy: 'UAH',
    buy: 1,
    sale: 1,
  })
  const rates = useSelector(selectRates)
  const status = useSelector(selectStatus)
  const error = useSelector(selectError)
  const dispatch = useDispatch()

  useEffect(() => {
    if (status === 'initial') {
      dispatch(setStatus('idle'))
    }
    if (status === 'failed') {
      console.warn('Error fetching rates data:')
      console.error(error)

      setApiMessage('Unavailable.')
    }
    if ((status === 'idle' || status === 'failed') && !rates.length) {
      console.info('Loading rates data...')

      dispatch(setStatus('loading'))
      dispatch(fetchRatesAsync())
    }
    if (status === 'idle' && rates.length) {
      let avgRates = [baseCurrency.current, ...rates].map((ccy) => ({
        ...ccy,
        sale: Number(ccy.sale),
        buy: Number(ccy.buy),
        avg: (Number(ccy.sale) + Number(ccy.buy)) / 2,
      }))
      avgRates = avgRates.filter((ccy) => ccy.ccy !== 'BTC')

      const list = avgRates.map((ccy) => ccy.ccy)

      setCurrencies(list)
      setUSD(avgRates.find((c) => c.ccy === 'USD'))
      setEUR(avgRates.find((c) => c.ccy === 'EUR'))
      setCurrenciesData(avgRates)
      setApiMessage('')

      console.info('Rates data loaded.')
    }
  }, [baseCurrency, rates, status, error, dispatch])

  const [apiMessage, setApiMessage] = useState('Loading...')
  const [currenciesData, setCurrenciesData] = useState([])
  const [currency1, setCurrency1] = useState('UAH')
  const [currency2, setCurrency2] = useState('UAH')
  const [amount1, setAmount1] = useState(100)
  const [amount2, setAmount2] = useState(100)
  const [preciseAmount1, setPreciseAmount1] = useState(100)
  const [preciseAmount2, setPreciseAmount2] = useState(100)
  const [activeInput, setActiveInput] = useState(0)
  const [editedInput, setEditedInput] = useState(1)
  const [exchangeDirection, setExchangeDirection] = useState(0)
  const [currencies, setCurrencies] = useState(['UAH'])
  const [USD, setUSD] = useState({ ccy: 'USD' })
  const [EUR, setEUR] = useState({ ccy: 'EUR' })

  const currencyExchange = useCallback(
    (srcCurrency, amount, trgCurrency) => {
      const [srcCurrencyData, trgCurrencyData] = [
        srcCurrency,
        trgCurrency,
      ].map((ccy) => currenciesData.find((c) => c.ccy === ccy))

      if (srcCurrency === trgCurrency) return amount
      else {
        const avgCrossRate = srcCurrencyData.avg / trgCurrencyData.avg
        return amount * avgCrossRate
      }
    },
    [currenciesData],
  )

  useEffect(() => {
    if (activeInput === 1 || (activeInput === 0 && editedInput === 1))
      setExchangeDirection(1)
    else if (activeInput === 2 || (activeInput === 0 && editedInput === 2))
      setExchangeDirection(2)
  }, [activeInput, editedInput])

  useEffect(() => {
    if (currenciesData.length) {
      let params
      if (exchangeDirection === 1) {
        params = [
          preciseAmount1,
          currency1,
          currency2,
          setPreciseAmount2,
          setAmount2,
        ]
      }
      if (exchangeDirection === 2) {
        params = [
          preciseAmount2,
          currency2,
          currency1,
          setPreciseAmount1,
          setAmount1,
        ]
      }

      ;((p) => {
        const [amount, ccy1, ccy2, setPreciseAmount, setAmount] = p
        const calculatedAmount = currencyExchange(ccy1, amount, ccy2)
        setPreciseAmount(calculatedAmount)
        setAmount(
          Number(calculatedAmount * 100 - ((calculatedAmount * 100) % 1)) / 100,
        )
      })(params)
    }
  }, [
    currenciesData,
    exchangeDirection,
    currency1,
    currency2,
    amount1,
    amount2,
    preciseAmount1,
    preciseAmount2,
    currencyExchange,
  ])

  const changeHandler = (changeTarget, value) => {
    switch (changeTarget) {
      case 'AMOUNT_1':
        setAmount1(value)
        setPreciseAmount1(value)
        setEditedInput(1)
        break
      case 'AMOUNT_2':
        setAmount2(value)
        setPreciseAmount2(value)
        setEditedInput(2)
        break
      case 'CURRENCY_1':
        setCurrency1(currencies[value])
        break
      case 'CURRENCY_2':
        setCurrency2(currencies[value])
        break
      default:
        break
    }
  }

  const amountsInputs = [amount1, amount2].map((value, key) => (
    <input
      key={key}
      type="number"
      className={`${
        exchangeDirection === key + 1 ? styles['active-input'] : ''
      } ${styles[`field-${key + 1}`]}`}
      onChange={(e) => changeHandler(`AMOUNT_${key + 1}`, e.target.value)}
      onFocus={() => {
        setActiveInput(key + 1)
      }}
      value={value}
    />
  ))

  const currencySeletcts = [1, 2].map((value) => (
    <select
      className={`${styles['list-' + value]} list-${value}`}
      onChange={(e) => changeHandler('CURRENCY_' + value, e.target.value)}
    >
      {currencies.map((currency, key) => (
        <option key={key} value={key}>
          {currency}
        </option>
      ))}
    </select>
  ))

  const ratesValues = [USD, EUR].map((ccy) => (
    <span key={ccy.ccy} className={styles['rate']}>
      <span
        key={`title_${ccy.ccy}`}
        className={styles[`${ccy.ccy.toLowerCase()}-rate-title`]}
      >
        {ccy.ccy}:
      </span>
      <span key={`value_${ccy.ccy}`} className={styles['rate-value']}>
        {ccy.avg
          ? (Number(ccy.avg) * 1000000 - ((Number(ccy.avg) * 1000000) % 1)) /
            1000000
          : apiMessage}
      </span>
    </span>
  ))

  const flagsBackgrounds = [
    { postfix: 'left', value: currency1 },
    { postfix: 'right', value: currency2 },
  ].map((flag, key) => (
    <img
      key={key}
      src={flags}
      className={
        styles['country-flag-' + flag.postfix] + ` ${styles[flag.value]}`
      }
      alt="country"
    />
  ))

  return (
    <div className={styles['widget']}>
      <div className={styles['widget-header']}>
        <img
          src={ratesSourceLogo}
          className={styles['rates-source-logo']}
          alt="logo"
        />
        {ratesValues}
      </div>

      <div className={styles['widget-content']}>
        {flagsBackgrounds}
        <svg
          className={
            styles[`field-bg-${exchangeDirection}`] + ' ' + styles['field-bg']
          }
          width="250px"
          height="50px"
          viewBox="0 0 250 50"
        >
          <path
            style={{ stroke: 'rgba(0,0,0,.15)', fill: 'rgba(0,10,0,.15)' }}
            d="M0,0 L230,0 C240,-3 245,20 250,25 C247,25 240,53 230,50 L0,50 L0,0Z"
          />
        </svg>
        <div className={styles['fields']}>
          {currencySeletcts[0]}
          {amountsInputs}
          {currencySeletcts[1]}
        </div>
      </div>
      <div className={styles['widget-footer']}>
        <p className={styles['footer-content']}>
          Конвертация производится по усредненному безналичному курсу АО КБ
          "Приватбанк"
          https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11
        </p>
      </div>
    </div>
  )
}
