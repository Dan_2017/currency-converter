import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { fetchRates } from './ConverterAPI'

const initialState = {
  rates: [],
  status: 'initial',
  error: null,
}

export const fetchRatesAsync = createAsyncThunk(
  'converter/fetchRates',
  async (addr) => {
    const response = await fetchRates(addr)
    return response
  },
)

export const converterSlice = createSlice({
  name: 'converter',
  initialState,
  reducers: {
    setStatus: (state, action) => {
      state.status = action.payload
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchRatesAsync.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchRatesAsync.fulfilled, (state, action) => {
        state.status = 'idle'
        state.rates = action.payload
      })
      .addCase(fetchRatesAsync.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action

        // Data mockup 24.08.2022

        // state.rates = [
        //   {
        //     ccy: 'USD',
        //     base_ccy: 'UAH',
        //     buy: '36.56860',
        //     sale: '37.31343',
        //   },
        //   {
        //     ccy: 'EUR',
        //     base_ccy: 'UAH',
        //     buy: '36.29800',
        //     sale: '37.17472',
        //   },
        //   {
        //     ccy: 'RUR',
        //     base_ccy: 'UAH',
        //     buy: '0.32000',
        //     sale: '0.35001',
        //   },
        //   {
        //     ccy: 'BTC',
        //     base_ccy: 'USD',
        //     buy: '20403.8814',
        //     sale: '22551.6584',
        //   },
        // ]
      })
  },
})

export const { setStatus } = converterSlice.actions

export const selectCount = (state) => state.converter.value
export const selectRates = (state) => state.converter.rates
export const selectStatus = (state) => state.converter.status
export const selectError = (state) => state.converter.error

export default converterSlice.reducer
