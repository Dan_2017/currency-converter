import { fetchRates } from './ConverterAPI'

describe('Converter rates data API', () => {
  it('should handle async request and receive rates data object', async () => {
    expect(typeof (await fetchRates())).toEqual('object')
  })

  it('should handle async request and receive rates data object with correct data structure', async () => {
    expect(Object.keys((await fetchRates())[0])).toEqual([
      'ccy',
      'base_ccy',
      'buy',
      'sale',
    ])
  })
})
