export async function fetchRates(
  addr = 'https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11',
) {
  const data = await fetch(addr)
  const json = await data.json()
  //   throw new Error('Data unavailable')
  await delay(2000)
  return json
}

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}
