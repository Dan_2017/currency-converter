import './App.css'
import { Converter } from './features/converter/Converter'
import { Wordchart } from './features/wordchart/Wordchart'

function App() {
  return (
    <div className="App">
      <Wordchart />
      <Converter />
    </div>
  )
}

export default App
