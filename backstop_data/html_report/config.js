report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/Testtask_Testtask_Homepage_0_document_0_phone.png",
        "test": "../bitmaps_test/20220831-155946/Testtask_Testtask_Homepage_0_document_0_phone.png",
        "selector": "document",
        "fileName": "Testtask_Testtask_Homepage_0_document_0_phone.png",
        "label": "Testtask Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "http://localhost:3001",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/Testtask_Testtask_Homepage_0_document_1_tablet.png",
        "test": "../bitmaps_test/20220831-155946/Testtask_Testtask_Homepage_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "Testtask_Testtask_Homepage_0_document_1_tablet.png",
        "label": "Testtask Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "http://localhost:3001",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    }
  ],
  "id": "Testtask"
});